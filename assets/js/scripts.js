(function($) {
  $('.ficha-slider').slick({
    arrows: true,
    dots: true,
    prevArrow: '<img src="assets/images/ficha-slider-left.png" class="ficha-prev" />',
    nextArrow: '<img src="assets/images/ficha-slider-right.png" class="ficha-next" />'
  });
})(jQuery);